package nl.hr.util.hibernate;

import nl.hr.model.MeasurementEntity;
import nl.hr.model.NodeEntity;
import nl.hr.model.UserEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * Created by bart on 24-10-16.
 */
class HibernateUtil {
    private static final SessionFactory sessionFactory;
    private static final ServiceRegistry serviceRegistry;

    static {
        try {
            Configuration configuration = new Configuration();

            try {
                configuration.configure("/config/hibernate.cfg.local.xml");
            } catch (Exception e) {
                configuration.configure("/config/hibernate.cfg.xml");
            }

//            Add entities to mapping
            configuration.addAnnotatedClass(UserEntity.class);
            configuration.addAnnotatedClass(MeasurementEntity.class);
            configuration.addAnnotatedClass(NodeEntity.class);

            serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() {
        return sessionFactory.openSession();
    }
}