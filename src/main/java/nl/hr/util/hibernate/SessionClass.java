package nl.hr.util.hibernate;

import org.hibernate.Session;

/**
 * Created by bart on 24-10-16.
 */
public class SessionClass {

    private static Session session = HibernateUtil.getSession();

    public static Session getSession() {
        if (session != null && session.isOpen() && session.isConnected()) {
            return session;
        } else {
            session = HibernateUtil.getSession();
            return session;
        }
    }
}