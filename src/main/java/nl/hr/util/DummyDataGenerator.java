package nl.hr.util;

import nl.hr.controller.NodeController;
import nl.hr.model.NodeEntity;
import org.json.JSONException;
import org.json.JSONObject;
import org.apache.commons.codec.binary.Base64;

import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by bart on 2-10-16.
 */
public class DummyDataGenerator {
    private double temp = 5;
    private double turbidity = 8;

    public DummyDataGenerator() {

        // And From your main() method or any other method
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                generateData();
            }
        }, 0, 5000);
    }

    private void generateData() {
        JSONObject jsonData = new JSONObject();
        try {
            jsonData = new JSONObject("{\"metadata\":{\"gateways\":[{\"rssi\":-39,\"snr\":7.8,\"rf_chain\":1,\"channel\":2,\"time\":\"\",\"gtw_id\":\"eui-1dee082e8dc10252\",\"timestamp\":3026317355}],\"data_rate\":\"SF7BW125\",\"modulation\":\"LORA\",\"coding_rate\":\"4/5\",\"time\":\"2017-01-16T15:16:33.533050362Z\",\"frequency\":868.5},\"port\":1,\"payload_raw\":\"TDo0MThUOjgwMA==\",\"counter\":0}");
            JSONObject metaData = jsonData.getJSONObject("metadata");
            metaData.put("time", LocalDateTime.now().toString());
        } catch (JSONException ignored) {
        }

        this.temp = addRandomTemp(this.temp);
        this.turbidity = addRandomTurbidity(this.turbidity);

        try {
            String payload = "TE:" + this.turbidity + ",TU:" + this.temp;
            payload = Base64.encodeBase64String(payload.getBytes());
            jsonData.put("payload_raw", payload);
        } catch (JSONException ignored) {
        }

        pushData(jsonData);
    }

    private double addRandomTemp(double temp) {
        double addTemp = getRandomNumberBetween(0, 1);
        if((getNotRandomBool() || temp > 25) && temp > -5) {
            addTemp = getRandomNumberBetween(-1, 0);
        }
        return ((double) Math.round((temp + addTemp) * 100) / 100);
    }

    private double addRandomTurbidity(double turbidity) {
        double addPh = getRandomNumberBetween(0, 0.1);
        if((getNotRandomBool() || turbidity > 8.4) && turbidity > 7.5) {
            addPh = getRandomNumberBetween(-0.1, 0);
        }

        return ((double) Math.round((turbidity + addPh) * 100) / 100);
    }

    private void pushData(JSONObject data) {
//        Call onMessage method of first node that we have, if we have one
        Iterator<Map.Entry<Integer, NodeEntity>> nodeIt = NodeController.nodes.entrySet().iterator();
        if(nodeIt.hasNext()){
            nodeIt.next().getValue().onMessage(data);
        }
    }

    private boolean getNotRandomBool() {
        Random random = new Random();
        return !random.nextBoolean();
    }

    private double getRandomNumberBetween(double min, double max) {
        return min + (Math.random() * (max - min));
    }
}
