package nl.hr.util;

import org.apache.velocity.app.VelocityEngine;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.template.velocity.VelocityTemplateEngine;

import java.util.Map;

public class ViewUtil {

    // Renders a template given a model and a request
    // The request is needed to check the user session for language settings
    // and to see if the user is logged in
    public static String render(Request request, Map<String, Object> model, String templatePath) {
        model.put("msg", request.attribute("messageBundle"));
        if(request.session().attribute("currentUser") != null) {
            model.put("currentUser", request.session().attribute("currentUser"));
        }
        model.put("WebPath", Path.Web.class); // Access application URLs from templates
        model.put("request", request); // Access application URLs from templates
        model.put("CSRFToken", request.attribute("CSRFToken"));
        return strictVelocityEngine().render(new ModelAndView(model, templatePath));
    }

    public static final Route homeRedirect = (Request request, Response response) -> {
        response.redirect(Path.Web.INDEX);
        return false;
    };

    private static VelocityTemplateEngine strictVelocityEngine() {
        VelocityEngine configuredEngine = new VelocityEngine();
        configuredEngine.setProperty("runtime.references.strict", true);
        configuredEngine.setProperty("resource.loader", "class");
        configuredEngine.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        return new VelocityTemplateEngine(configuredEngine);
    }
}
