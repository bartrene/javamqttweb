package nl.hr.util;

import java.math.BigDecimal;
import java.util.regex.Pattern;

/**
 * Created by bart on 16-10-16.
 */
public class DataTypeUtil {
    public static boolean stringIsInt(String str) {
        return str.matches("^[+-]?\\d+$");
    }

    public static boolean stringIsLatOrLong(String str) {
        Pattern regexPattern = Pattern.compile("^(-?\\d+(\\.\\d+)?),\\s*(\\-?\\d+(\\.\\d+)?)$");
        return regexPattern.matcher(str).matches();
    }

    public static BigDecimal stringtoBigDecimal(String str) {
        str = str.replaceAll(",","");
        return new BigDecimal(str);
    }
}
