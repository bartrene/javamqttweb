package nl.hr.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

/**
 * Created by bart on 24-1-17.
 */
public class PropertyUtil extends Properties {
    private static PropertyUtil instance = null;

    private String propLocation = "src/main/resources/config/config.local.properties";

    private PropertyUtil() {

        // Load the properties file
        try {
            this.load(new FileInputStream(propLocation));
        } catch (Exception e) {
            try {
                propLocation = "src/main/resources/config/config.properties";
                this.load(new FileInputStream(propLocation));
            } catch (IOException ignored) {
                new Exception("ConfigException, configuration file not found.").printStackTrace();
            }
        }
    }

    public void store() throws IOException {
        OutputStream out = new FileOutputStream(this.propLocation);
        super.store(out, "Stored from the PropertyUtil class");
    }

    public static PropertyUtil getInstance() {
        if(instance == null) {
            instance = new PropertyUtil();
        }
        return instance;
    }
}
