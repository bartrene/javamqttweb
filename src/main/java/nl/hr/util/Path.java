package nl.hr.util;

import lombok.Getter;

public class Path {

    // The @Getter methods are needed in order to access
    // the variables from Velocity Templates
    public static class Web {
        @Getter public static final String HOME = "/";
        @Getter public static final String INDEX = "/index/";
        @Getter public static final String PREDICTION = "/prediction/";
        @Getter public static final String NODES = "/nodes/";
        @Getter public static final String LOGIN = "/login/";
        @Getter public static final String LOGOUT = "/logout/";
    }

    public static class Template {
        public final static String INDEX = "/velocity/pages/index.vm";
        public final static String PREDICTION = "/velocity/pages/prediction.vm";
        public final static String NODES = "/velocity/pages/nodes.vm";
        public final static String LOGIN = "/velocity/login/login.vm";
    }

}
