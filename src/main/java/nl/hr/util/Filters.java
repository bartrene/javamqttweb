package nl.hr.util;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.apache.commons.lang3.RandomStringUtils;
import spark.Filter;
import spark.Request;
import spark.Response;

import java.security.SecureRandom;
import java.util.concurrent.TimeUnit;

public class Filters {

    // If a user manually manipulates paths and forgets to add
    // a trailing slash, redirect the user to the correct path
    public static final Filter addTrailingSlashes = (Request request, Response response) -> {
        if (!request.pathInfo().endsWith("/")) {
            response.redirect(request.pathInfo() + "/");
        }
    };

    // Locale change can be initiated from any page
    // The locale is extracted from the request and saved to the user's session
    public static final Filter handleLocaleChange = (Request request, Response response) -> {
        if (request.queryParams("locale") != null) {
            request.session().attribute("locale", request.queryParams("locale"));
            response.redirect(request.pathInfo());
        }
    };

    // Enable GZIP for all responses
    public static final Filter addGzipHeader = (Request request, Response response) -> response.header("Content-Encoding", "gzip");

    public static final Filter addCSRFToken = (Request request, Response response) -> {
        // Check the user session for the salt cache, if none is present we create one
        Cache<String, Boolean> csrfPreventionSaltCache = request.session().attribute("CSRFTokenCache");

        if (csrfPreventionSaltCache == null){
            csrfPreventionSaltCache = CacheBuilder.newBuilder()
                    .maximumSize(5000)
                    .expireAfterWrite(20, TimeUnit.MINUTES)
                    .build();
            request.session().attribute("CSRFTokenCache", csrfPreventionSaltCache);
        }

        // Generate the salt and store it in the users cache
        String salt = RandomStringUtils.random(20, 0, 0, true, true, null, new SecureRandom());
        csrfPreventionSaltCache.put(salt, Boolean.TRUE);

        // Add the salt to the current request so it can be used
        // by the page rendered in this request
        request.attribute("CSRFToken", salt);

    };
    public static final Filter addMessageBundle = (Request request, Response response) -> request.attribute("messageBundle", new MessageBundle(request.session().attribute("locale")));
}
