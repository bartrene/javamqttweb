package nl.hr.util;

import com.google.common.cache.Cache;
import spark.Request;

import static spark.Spark.halt;

public class RequestUtil {


    public static boolean removeSessionAttrLoggedOut(Request request) {
        Object loggedOut = request.session().attribute("loggedOut");
        request.session().removeAttribute("loggedOut");
        return loggedOut != null;
    }

    public static void validateCSRFToken(Request request) {
        // Get the salt sent with the request
        String salt = request.queryParams("CSRFToken");

        // Validate that the salt is in the cache
        Cache<String, Boolean> csrfPreventionSaltCache = request.session().attribute("CSRFTokenCache");

        if (csrfPreventionSaltCache == null || salt == null || csrfPreventionSaltCache.getIfPresent(salt) == null){
            // Otherwise we halt the request flow
            halt(401, "Potential CSRF detected!! Inform a scary sysadmin ASAP.");
        }
    }
}
