package nl.hr.dao;

import nl.hr.Application;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerMinMaxScaler;
import org.nd4j.linalg.dataset.api.preprocessor.serializer.NormalizerMinMaxScalerSerializer;
import org.nd4j.linalg.factory.Nd4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Bart on 06/11/2017.
 */
public class PredictionDao {
    private static final File networkSaveLocation = new File(Application.props.getProperty("predictions.networklocation"));
    private static final File normalizerSaveLocation = new File(Application.props.getProperty("predictions.normalizerlocation"));
    private static final File featureMatrixSaveLocation = new File(Application.props.getProperty("predictions.featurematrixlocation"));

    public static void saveFeatureMatrixToFile(INDArray matrix) {
        if(Application.debug){
            System.out.println("Saving featureMatrix to: " + featureMatrixSaveLocation);
        }
        try {
            FileOutputStream out = new FileOutputStream(featureMatrixSaveLocation);
            Nd4j.write(out, matrix);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveNetworkToFile(MultiLayerNetwork net) {
        if(Application.debug){
            System.out.println("Closing and saving network in: " + networkSaveLocation);
        }
        try {
            ModelSerializer.writeModel(net, networkSaveLocation, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveNormalizerToFile(NormalizerMinMaxScaler norm) {
        if(Application.debug){
            System.out.println("Closing and saving normalizer in: " + normalizerSaveLocation);
        }
        try {
            NormalizerMinMaxScalerSerializer.write(norm, normalizerSaveLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static NormalizerMinMaxScaler loadNormalizerFromFile() {
        try {
            if(Application.debug) {
                System.out.println("Loading normalizer from: " + normalizerSaveLocation);
            }
            return NormalizerMinMaxScalerSerializer.restore(normalizerSaveLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static MultiLayerNetwork loadNetworkFromFile() {
        try {
            if(Application.debug) {
                System.out.println("Loading Network from: " + networkSaveLocation);
            }
            return ModelSerializer.restoreMultiLayerNetwork(networkSaveLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static INDArray loadFeatureMatrixFromFile() {
        if(Application.debug){
            System.out.println("Loading featureMatrix from: " + featureMatrixSaveLocation);
        }
        try {
            FileInputStream in = new FileInputStream(featureMatrixSaveLocation);
            return Nd4j.read(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean normalizerExists() {
        return normalizerSaveLocation.exists() && !normalizerSaveLocation.isDirectory();
    }

    public static boolean networkExists() {
        return networkSaveLocation.exists() && !networkSaveLocation.isDirectory();
    }
}
