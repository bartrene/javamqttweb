package nl.hr.dao;

import nl.hr.model.MeasurementEntity;
import org.hibernate.query.Query;

import java.sql.Timestamp;
import java.util.ArrayList;

import static nl.hr.util.hibernate.SessionClass.getSession;

/**
 * Created by bart on 23/09/16.
 */
public class MeasurementDao extends MainDao {
    public MeasurementDao() {
        entityType = MeasurementEntity.class;
    }

    public ArrayList <Object[]> getCountPerDate(int limit) {
        Query query = getSession().createQuery("SELECT count(m.id), date(m.datetime) from nl.hr.model.MeasurementEntity m group by date(m.datetime) ORDER BY date(m.datetime) desc");
        if(limit != 0) {
            query.setMaxResults(limit);
        }
        return (ArrayList<Object[]>) new ArrayList(query.list());
    }

    public ArrayList <Double> getTemperature(int limit) {
        Query query = getSession().createQuery("SELECT m.temperature from nl.hr.model.MeasurementEntity m ORDER BY m.datetime desc");
        if(limit != 0) {
            query.setMaxResults(limit);
        }
        return (ArrayList<Double>) new ArrayList(query.list());
    }

    public ArrayList<Double> getTempsBeforeTime(Timestamp datetime) {
        Query query = getSession().createQuery("SELECT m.temperature from nl.hr.model.MeasurementEntity m WHERE m.datetime < :datetime ORDER BY m.datetime desc");
        query.setParameter("datetime", datetime);
        return (ArrayList<Double>) query.list();
    }

}
