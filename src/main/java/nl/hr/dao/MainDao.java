package nl.hr.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;

import static nl.hr.util.hibernate.SessionClass.getSession;

/**
 * Created by bart on 23/09/16.
 */
public class MainDao {
    Class entityType;

    public void saveEntity(Object entity) {
        try (Session session = getSession()) {
            Transaction ts = session.beginTransaction();
            session.save(entity);
            ts.commit();
        }
    }

    @SuppressWarnings("unused")
    public void updateEntity(Object entity) {
        try (Session session = getSession()) {
            Transaction ts = session.beginTransaction();
            session.update(entity);
            ts.commit();
        }
    }

    public void saveOrUpdateEntity(Object entity) {
        try (Session session = getSession()) {
            Transaction ts = session.beginTransaction();
            session.saveOrUpdate(entity);
            ts.commit();
        }
    }

    @SuppressWarnings("WeakerAccess")
    public void refreshEntity(Object entity){
        try (Session session = getSession()) {
            session.refresh(entity);
        }
    }
    public void deleteEntityById(int entityId) {
        try (Session session = getSession()) {
            Transaction ts = session.beginTransaction();
            Object entity = session.load(entityType, entityId);
            session.delete(entity);
            ts.commit();
        }
    }

    public Object getEntityById(int entityId) {
        return getSession().load(entityType, entityId);
    }

    public ArrayList getAll(int limit) {
        Query query = getSession().createQuery("from " +  entityType.getName());
        if(limit != 0) {
            query.setMaxResults(limit);
        }
        return new ArrayList(query.list());
    }
}