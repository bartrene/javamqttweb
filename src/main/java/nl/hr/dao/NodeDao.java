package nl.hr.dao;

import nl.hr.controller.NodeController;
import nl.hr.model.NodeEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.time.LocalDateTime;

import static nl.hr.util.hibernate.SessionClass.getSession;

/**
 * Created by bart on 23/09/16.
 */
public class NodeDao extends MainDao {

    public NodeDao() {
        entityType = NodeEntity.class;
    }

    public void updateLastReceived(NodeEntity nodeEntity) {
        refreshEntity(nodeEntity);
        nodeEntity.setLastReceived(LocalDateTime.now());
        try (Session session = getSession()) {
            Transaction ts = session.beginTransaction();
            session.update(nodeEntity);
            ts.commit();
        }
    }

    public void insertNode(NodeEntity node) {
        if(this.checkAppIdAvailability(node.getAppid())) {
            try (Session session = getSession()) {
                Transaction ts = session.beginTransaction();
                session.save(node);
                ts.commit();
                NodeController.nodes.put(node.getId(), node);
                node.register();
            }
        }
    }

    public void updateEntity(Object entity) {
        try (Session session = getSession()) {
            Transaction ts = session.beginTransaction();
            session.update(entity);
            ts.commit();
            NodeController.nodes.put(((NodeEntity) entity).getId(), (NodeEntity) entity);
            ((NodeEntity) entity).reRegister();
        }
    }

    private boolean checkAppIdAvailability(String appid) {
        try (Session session = getSession()) {
            Query query = getSession().createQuery("select count(*) from " +  entityType.getName() + " where appid = :appid");
            query.setString("appid", appid);
            return (Long)query.uniqueResult() < 1;
        }
    }
}
