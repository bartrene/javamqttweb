package nl.hr.dao;

import nl.hr.model.UserEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import static nl.hr.util.hibernate.SessionClass.getSession;

/**
 * Created by bart on 23/09/16.
 */
public class UserDao extends MainDao {

    public UserEntity getUserByUsername(String username) {
        try (Session session = getSession()) {
            Criteria criteria = session.createCriteria(UserEntity.class);
            return (UserEntity) criteria.add(Restrictions.eq("username", username)).uniqueResult();
        }
    }
}
