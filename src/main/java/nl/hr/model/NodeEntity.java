package nl.hr.model;

import nl.hr.Application;
import nl.hr.controller.DataWebSocketHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.thethingsnetwork.data.common.Connection;
import org.thethingsnetwork.data.mqtt.Client;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.apache.commons.codec.binary.Base64;
import static nl.hr.Application.measurementDao;
import static nl.hr.Application.nodeDao;

/**
 * Created by bart on 10-10-16.
 */
@SuppressWarnings({"RedundantIfStatement", "unused"})
@Entity
@Table(name = "node", schema = "waterquality")
public class NodeEntity {
    private int id;
    private String appid;
    private String appaccesskey;
    private BigDecimal latitude;
    private BigDecimal longitude;
    private LocalDateTime lastReceived;

    @Transient
    private Client mqttClient;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "appid", nullable = false, length = 32)
    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    @Basic
    @Column(name = "appaccesskey", nullable = false, length = 64)
    public String getAppaccesskey() {
        return appaccesskey;
    }

    public void setAppaccesskey(String appaccesskey) {
        this.appaccesskey = appaccesskey;
    }

    @Basic
    @Column(name = "latitude", nullable = false, precision = 9, scale = 6)
    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    @Basic
    @Column(name = "longitude", nullable = false, precision = 9, scale = 6)
    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    @Basic
    @Column(name = "last_received")
    public LocalDateTime getLastReceived() {
        return lastReceived;
    }

    @Transient
    private long getLastReceivedAsEpoch() {
        if(lastReceived == null) return 0;
        else {
            return lastReceived.atZone(ZoneId.systemDefault()).toEpochSecond();
        }
    }

    public void setLastReceived(LocalDateTime lastReceived) {
        this.lastReceived = lastReceived;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NodeEntity that = (NodeEntity) o;

        if (id != that.id) return false;
        if (appid != null ? !appid.equals(that.appid) : that.appid != null) return false;
        if (appaccesskey != null ? !appaccesskey.equals(that.appaccesskey) : that.appaccesskey != null) return false;
        if (latitude != null ? !latitude.equals(that.latitude) : that.latitude != null) return false;
        if (longitude != null ? !longitude.equals(that.longitude) : that.longitude != null) return false;
        if (lastReceived != null ? !lastReceived.equals(that.lastReceived) : that.lastReceived != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (appid != null ? appid.hashCode() : 0);
        result = 31 * result + (appaccesskey != null ? appaccesskey.hashCode() : 0);
        result = 31 * result + (latitude != null ? latitude.hashCode() : 0);
        result = 31 * result + (longitude != null ? longitude.hashCode() : 0);
        result = 31 * result + (lastReceived != null ? lastReceived.hashCode() : 0);
        return result;
    }

    @Transient
    public void register() {
        try {
            mqttClient = new Client("eu", this.appid, this.appaccesskey)
                    .onMessage((String devId, Object data) -> this.onMessage(data))
                    .onError(this::onError)
                    .onConnected(this::onConnection)
                    .start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transient
    public void reRegister() {
        try {
            if(mqttClient != null) {
                mqttClient.end();
            }
            this.register();
        } catch (Exception e) {
            System.out.println("Could not re-register Node");
        }

    }

    @Transient
    public JSONObject getAsJson() {
        JSONObject json = new JSONObject();

        try {
            json.put("id", this.id);
            json.put("appid", this.appid);
            json.put("appaccesskey", this.appaccesskey);
            json.put("latitude", this.latitude);
            json.put("longitude", this.longitude);
            json.put("lastReceived", getLastReceivedAsEpoch());
        } catch (JSONException ignored) {
        }

        return json;
    }

    @Transient
    public void onMessage(Object m) {
        saveData((JSONObject) m);

        try {
            ((JSONObject)((JSONObject) m).get("metadata")).put("nodeid", this.id);
        } catch (JSONException ignored) {
        }

        DataWebSocketHandler.broadcastMessage((JSONObject) m);
        Application.tempPredictionController.onNewMeasurement();
        nodeDao.updateLastReceived(this);
    }

    @Transient
    private void onError(Throwable t) {
        System.out.println("An error occured for device: " + this.getId() + " :");
        t.printStackTrace();
    }

    @Transient
    private void onConnection (Connection c) {
        System.out.println("We connected: " + this.getId());
    }

    @Transient
    private void saveData(JSONObject data) {
        MeasurementEntity measurement = new MeasurementEntity();
        try {
            JSONObject metaData = (JSONObject) data.get("metadata");
            JSONObject firstGateWay = ((JSONArray) metaData.get("gateways")).getJSONObject(0);

            measurement.setGateway((String) firstGateWay.get("gtw_id"));
            measurement.setNode(this.id);

            Timestamp timestamp = Timestamp.valueOf(LocalDateTime.parse((String) metaData.get("time"), DateTimeFormatter.ISO_DATE_TIME));
            measurement.setDatetime(timestamp);

            String payload = new String(Base64.decodeBase64(((String) data.get("payload_raw"))), "UTF-8").replaceAll(".*L:(|T:).*", "");
            String[] values = payload.split(",");
            double temp = Double.parseDouble(values[0].substring(values[0].lastIndexOf(":") + 1));
            double turbidity = Double.parseDouble(values[1].substring(values[1].lastIndexOf(":") + 1));

            measurement.setTemperature(temp);
            measurement.setTurbidity(turbidity);
        } catch (Exception e) {
            e.printStackTrace();
        }

        measurementDao.saveEntity(measurement);
    }
}
