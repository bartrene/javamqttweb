package nl.hr.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by bart on 3-10-16.
 */
@SuppressWarnings({"RedundantIfStatement", "unused"})
@Entity
@Table(name = "measurement", schema = "waterquality")
public class MeasurementEntity {
    private int id;
    private String gateway;
    private int node;
    private Timestamp datetime;
    private Double temperature;
    private Double turbidity;

    @SuppressWarnings("unused")
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "gateway")
    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    @Basic
    @Column(name = "node")
    public int getNode() {
        return node;
    }

    public void setNode(int node) {
        this.node = node;
    }

    @Basic
    @Column(name = "datetime")
    public Timestamp getDatetime() {
        return datetime;
    }

    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }

    @Basic
    @Column(name = "temperature")
    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    @Basic
    @Column(name = "turbidity")
    public Double getTurbidity() {
        return turbidity;
    }

    public void setTurbidity(Double turbidity) {
        this.turbidity = turbidity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MeasurementEntity that = (MeasurementEntity) o;

        if (id != that.id) return false;
        if (gateway != null ? !gateway.equals(that.gateway) : that.gateway != null) return false;
        if (node != that.node) return false;
        if (datetime != null ? !datetime.equals(that.datetime) : that.datetime != null) return false;
        if (temperature != null ? !temperature.equals(that.temperature) : that.temperature != null) return false;
        if (turbidity != null ? !turbidity.equals(that.turbidity) : that.turbidity != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (gateway != null ? gateway.hashCode() : 0);
        result = 31 * result + node;
        result = 31 * result + (datetime != null ? datetime.hashCode() : 0);
        result = 31 * result + (temperature != null ? temperature.hashCode() : 0);
        result = 31 * result + (turbidity != null ? turbidity.hashCode() : 0);
        return result;
    }


}
