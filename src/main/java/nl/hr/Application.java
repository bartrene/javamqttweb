package nl.hr;

import nl.hr.controller.*;
import nl.hr.dao.MeasurementDao;
import nl.hr.dao.NodeDao;
import nl.hr.dao.UserDao;
import nl.hr.util.*;
import spark.Spark;

import static spark.Spark.*;
import static spark.debug.DebugScreen.enableDebugScreen;

@SuppressWarnings("unused")
public class Application {
    public static final boolean debug = false;

    // Declare dependencies
    public static final UserDao userDao = new UserDao();
    public static final MeasurementDao measurementDao = new MeasurementDao();
    public static final NodeDao nodeDao = new NodeDao();

    //Declare settings
    public static final String hashPepper = "DCQpv6lE10";

    // Declare MQTT Clients
    public static NodeController nodeController = new NodeController();
    public static DummyDataGenerator dummy1 = new DummyDataGenerator();

    //Setup Properties
    public static final PropertyUtil props = PropertyUtil.getInstance();

    //Initialize DL4J network controller
    public static final TempPredictionController tempPredictionController = new TempPredictionController();

    public static void main(String[] args) {
        //    Use this to register a new user manually.
        //    UserController.registerUser("admin", "Welkom123");

        // Configure Spark
        port(Integer.parseInt(props.getProperty("port")));
        staticFiles.location("/public");
        staticFiles.expireTime(600L);

        if(debug) {
            enableDebugScreen();
        }

        // Set up websockets
        webSocket("/websocketdata", DataWebSocketHandler.class);
        init();
//
        before("*", Filters.addCSRFToken);
        before("*", Filters.addMessageBundle);
        before("*", Filters.addTrailingSlashes);
        before("*", Filters.handleLocaleChange);

        // Set up routes
        get(Path.Web.HOME, ViewUtil.homeRedirect);
        get(Path.Web.INDEX, DashboardController.serveDashboardPage);
        get(Path.Web.PREDICTION, TempPredictionController.servePredictionPage);
        get(Path.Web.NODES, NodeController.serveNodesPage);
        post(Path.Web.NODES, NodeController.postNodePage);
        get(Path.Web.LOGIN, LoginController.serveLoginPage);
        post(Path.Web.LOGIN, LoginController.handleLoginPost);
        get(Path.Web.LOGOUT,  LoginController.handleLogoutPost);

        //Set up after-filters (called after each get/post)
        after("*",  Filters.addGzipHeader);

        Spark.exception(Exception.class, (exception, request, response) -> exception.printStackTrace());
    }

}
