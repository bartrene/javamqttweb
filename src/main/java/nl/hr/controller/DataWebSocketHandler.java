package nl.hr.controller;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by bart on 25-9-16.
 *
 */
@SuppressWarnings("unused")
@WebSocket
public class DataWebSocketHandler {
    private String sender, msg;
    private static final ArrayList<Session> sessions = new ArrayList<>();

    @OnWebSocketConnect
    public void onConnect(Session session) {
        sessions.add(session);
    }

    @OnWebSocketClose
    public void onClose(Session session, int statusCode, String reason) {
        sessions.remove(session);
    }

    public static void broadcastMessage(JSONObject data) {
        sessions.forEach(session -> {
            try {
                session.getRemote().sendString(String.valueOf(data));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

}
