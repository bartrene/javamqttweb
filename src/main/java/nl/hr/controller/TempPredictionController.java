package nl.hr.controller;

import nl.hr.Application;
import nl.hr.dao.MeasurementDao;
import nl.hr.dao.PredictionDao;
import nl.hr.model.MeasurementEntity;
import nl.hr.util.Path;
import nl.hr.util.ViewUtil;
import org.datavec.api.records.reader.SequenceRecordReader;
import org.datavec.api.records.reader.impl.collection.CollectionSequenceRecordReader;
import org.datavec.api.writable.DoubleWritable;
import org.datavec.api.writable.Writable;
import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.datasets.datavec.SequenceRecordReaderDataSetIterator;
import org.deeplearning4j.eval.RegressionEvaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.GravesLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.DataSet;
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerMinMaxScaler;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import spark.Request;
import spark.Response;
import spark.Route;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

import static nl.hr.Application.props;
import static nl.hr.controller.LoginController.ensureUserIsLoggedIn;
import static nl.hr.util.RequestUtil.removeSessionAttrLoggedOut;

/**
 * Created by bart on 23-1-17.
 */
@SuppressWarnings("FieldCanBeLocal")
public class TempPredictionController {
    private final double trainPercentage = 0.65;
    private final int miniBatchSize = 144;



    private MultiLayerNetwork net;
    private NormalizerMinMaxScaler normalizer;
    private INDArray featureMatrix;

    private List<List<List<Writable>>> prepareMeasurementData(ArrayList<MeasurementEntity> measurements, int from, int to) {
        List<List<List<Writable>>> topSequences = new ArrayList<>();
        List<List<Writable>> listOfSequences = new ArrayList<>();

        for(int i=from;i < (to - 1);i++) {
            List<Writable> sequence = new ArrayList<>();
            sequence.add(new DoubleWritable(measurements.get(i).getTemperature()));
            listOfSequences.add(sequence);

        }

        topSequences.add(listOfSequences);
        return topSequences;
    }

    private DataSet getMeasurementIterator(ArrayList<MeasurementEntity> measurements, int from, int to) {
        List<List<List<Writable>>> sequenceTrainData1 = prepareMeasurementData(measurements, from, to);
        List<List<List<Writable>>> sequenceTrainData2 = prepareMeasurementData(measurements, from, to);
        SequenceRecordReader trainReader = new CollectionSequenceRecordReader(sequenceTrainData1);
        SequenceRecordReader trainReaderLabels = new CollectionSequenceRecordReader(sequenceTrainData2);
        return new SequenceRecordReaderDataSetIterator(trainReader, trainReaderLabels, miniBatchSize, -1, true).next();
    }

    public TempPredictionController() {
        if(PredictionDao.networkExists() && PredictionDao.normalizerExists() && !Application.debug) {
            net = PredictionDao.loadNetworkFromFile();
            featureMatrix = PredictionDao.loadFeatureMatrixFromFile();
            normalizer = PredictionDao.loadNormalizerFromFile();
        } else {
            MultiLayerNetwork newNet = this.getNewTrainedNetwork();
            if(newNet != null) {
                PredictionDao.saveNetworkToFile(newNet);
                net = newNet;
            }
        }


        if(Application.debug) {
//        Setup the UI server so we can monitor its progress
            StatsStorage statsStorage = new InMemoryStatsStorage();
            net.setListeners(new StatsListener(statsStorage));
            UIServer uiServer = UIServer.getInstance();
            uiServer.attach(statsStorage);
        }

    }

    @SuppressWarnings("WeakerAccess")
    public ArrayList<Double> getPrediction() {
        INDArray predicted = net.rnnTimeStep(featureMatrix);

        normalizer.revertLabels(predicted);
        predicted = predicted.getRow(0);

        ArrayList<Double> values = new ArrayList<>();
        for(int i=0;i<predicted.length();i++) {
            try {

                values.add(Double.parseDouble(predicted.getColumn(i).toString()));
            } catch (Exception e) {
                System.out.println(predicted.getColumn(i));
                System.out.println(predicted.getColumn(i).toString());

                e.printStackTrace();
                throw e;
            }
        }
        return values;
    }

    private MultiLayerNetwork getNewTrainedNetwork() {
        MeasurementDao measurementDao = new MeasurementDao();
        /*
        * For now, dont't use more than 2000 measurements otherwise we will be overfitting.
        * It's possible to use more but we would need to to more finetuning on the network to not
        * make the network overfit. We don't have time for that ;)
        * */
        ArrayList<MeasurementEntity> measurements = measurementDao.getAll(2000);

        if(measurements.size() < 1) {
            return null;
        }

        int totalSize = measurements.size();
        int trainSize = (int) Math.floor(totalSize*trainPercentage);

        props.setProperty("predictions.firsttime", String.valueOf(measurements.get(0).getDatetime().getTime()));
        props.setProperty("predictions.lasttime", String.valueOf(measurements.get(totalSize-1).getDatetime().getTime()));

        try {
            props.store();
        } catch (IOException e) {
            e.printStackTrace();
        }

        NormalizerMinMaxScaler normalizer = new NormalizerMinMaxScaler(0, 1);
//        Traindata
        DataSet trainData = getMeasurementIterator(measurements, 0, trainSize);

//        Testdata
        DataSet testData = getMeasurementIterator(measurements, trainSize, totalSize);

        if (Application.debug) {
            System.out.println("===DATA===");
            System.out.println(trainData);
            System.out.println(testData);
        }

        //Normalize data, including labels (fitLabel=true)
        normalizer.fitLabel(true);
        normalizer.fit(trainData);              //Collect training data statistics

        normalizer.transform(trainData);
        normalizer.transform(testData);

        if (Application.debug) {
            System.out.println("===Normalized===");
            System.out.println(trainData);
            System.out.println(testData);
        }

        MultiLayerNetwork net = this.setupNetwork();

        if(totalSize > 0 && trainData != null && testData != null) {
            this.trainNetwork(net, trainData, testData);
        }

        this.featureMatrix = testData != null ? testData.getFeatureMatrix() : null;
        this.normalizer = normalizer;

        PredictionDao.saveFeatureMatrixToFile(this.featureMatrix);

        PredictionDao.saveNormalizerToFile(this.normalizer);

        return net;
    }


    private void trainNetwork(MultiLayerNetwork net, DataSet trainData, DataSet testData) {
        System.out.println("===Starting to train the network===");
        // ----- Train the network, evaluating the test set performance at each epoch -----
        int nEpochs = 100;
        for (int i = 0; i < nEpochs; i++) {
            net.fit(trainData);
            if(Application.debug) {
                System.out.println("Epoch " + i + " complete. Time series evaluation:");
            }

            //Run regression evaluation on our single column input
            RegressionEvaluation evaluation = new RegressionEvaluation(1);
            INDArray features = testData.getFeatureMatrix();

            INDArray lables = testData.getLabels();
            INDArray predicted = net.output(features, false);

            evaluation.evalTimeSeries(lables, predicted);

            if(Application.debug) {
                System.out.println(predicted);
                //Just do sout here since the logger will shift the shift the columns of the stats
                System.out.println(evaluation.stats());
            }
        }

        //Init rrnTimeStemp with train data and predict test data
        net.rnnTimeStep(trainData.getFeatureMatrix());
        System.out.println("===Training done===");
    }

    private MultiLayerNetwork setupNetwork() {
        // Configure to Network
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(140)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .iterations(1)
                .weightInit(WeightInit.XAVIER)
                .updater(Updater.NESTEROVS).momentum(0.9)
                .learningRate(0.0001)
                .list()
                .layer(0, new GravesLSTM.Builder().activation(Activation.TANH).nIn(1).nOut(10)
                        .build())
                .layer(1, new RnnOutputLayer.Builder(LossFunctions.LossFunction.MSE)
                        .activation(Activation.IDENTITY).nIn(10).nOut(1).build())
                .pretrain(false)
                .build();
        MultiLayerNetwork net = new MultiLayerNetwork(conf);
        net.init();
        return net;
    }

    public static final Route servePredictionPage = (Request request, Response response) -> {
        ensureUserIsLoggedIn(request, response);
        MeasurementDao measurementDao = new MeasurementDao();

        Map<String, Object> model = new HashMap<>();
        model.put("loggedOut", removeSessionAttrLoggedOut(request));

        ArrayList<Double> originalTemps = new ArrayList<>();
        ArrayList<Double> predictedTemps = new ArrayList<>();
        ArrayList<Timestamp> labels = new ArrayList<>();

        String sFirsttime = props.getProperty("predictions.firsttime");
        String sLasttime = props.getProperty("predictions.lasttime");


        if(sFirsttime != null && sLasttime != null) {

            long firsttime = Long.parseLong(sFirsttime);
            long lasttime = Long.parseLong(sLasttime);

            model.put("firsttime", firsttime);
            model.put("lasttime", lasttime);


            // Original data
            originalTemps = measurementDao.getTempsBeforeTime(new Timestamp(lasttime));

            //Predicted data
            predictedTemps = Application.tempPredictionController.getPrediction();

            // Labels
            labels.add(new Timestamp(firsttime));
            labels.add(new Timestamp(lasttime));
            labels.add(new Timestamp((lasttime - firsttime) + lasttime));
        }

        model.put("originalTemps", originalTemps);
        model.put("predictedTemps", predictedTemps);
        model.put("labels", labels);

        return ViewUtil.render(request, model, Path.Template.PREDICTION);
    };

    public void onNewMeasurement() {
        String sMeasurementCount = Application.props.getProperty("predictions.measurementcount");
        if(sMeasurementCount == null) {
            sMeasurementCount = "0";
        }

        int measurementCount = Integer.parseInt(sMeasurementCount);
        measurementCount++;

        if(measurementCount >= 1000) {
            MultiLayerNetwork newNet = this.getNewTrainedNetwork();
            PredictionDao.saveNetworkToFile(newNet);
            net = newNet;
            measurementCount = 0;
        }

        Application.props.setProperty("predictions.measurementcount", String.valueOf(measurementCount));
        try {
            Application.props.store();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
