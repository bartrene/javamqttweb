package nl.hr.controller;

import nl.hr.dao.MeasurementDao;
import nl.hr.util.Path;
import nl.hr.util.ViewUtil;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static nl.hr.controller.LoginController.ensureUserIsLoggedIn;
import static nl.hr.util.RequestUtil.removeSessionAttrLoggedOut;

/**
 * Created by Bart on 9/25/2016.
 * ${CLASS_NAME}
 */
public class DashboardController {

    public static final Route serveDashboardPage = (Request request, Response response) -> {
        ensureUserIsLoggedIn(request, response);
        MeasurementDao measurementDao = new MeasurementDao();

        Map<String, Object> model = new HashMap<>();
        model.put("loggedOut", removeSessionAttrLoggedOut(request));

        ArrayList<Object[]> counterPerDate= measurementDao.getCountPerDate(2);
        model.put("counterPerDate", counterPerDate);
        model.put("counterPerDatePercentage", getCounterPerDatePercentage(counterPerDate));

        model.put("counterPerDateChart", measurementDao.getCountPerDate(30));

        model.put("temperatueChart", measurementDao.getTemperature(40));

        model.put("records", measurementDao.getAll(100));

        return ViewUtil.render(request, model, Path.Template.INDEX);
    };

    private static double getCounterPerDatePercentage(ArrayList<Object[]> data) {
        if(data.size() < 2) return 0.0;
        long val1 = (long) data.get(0)[0];
        long val2 = (long) data.get(1)[0];
        return ((double) Math.round((((double) (val1 - val2) / val2) * 100) * 100) / 100);
    }

}
