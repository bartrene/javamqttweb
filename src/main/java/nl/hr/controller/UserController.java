package nl.hr.controller;

import com.google.common.hash.Hashing;
import nl.hr.model.UserEntity;
import org.mindrot.jbcrypt.BCrypt;

import java.nio.charset.StandardCharsets;

import static nl.hr.Application.hashPepper;
import static nl.hr.Application.userDao;

class UserController {

    // Authenticate the user by hashing the inputted password using the stored salt,
    // then comparing the generated hashed password to the stored hashed password
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean authenticate(String username, String password) {

//        Make sure the details aren't empty
        if (username.isEmpty() || password.isEmpty()) {
            return false;
        }
//        Get the user so we can check his password
        UserEntity user = userDao.getUserByUsername(username);

        if(user == null) {
            return false;
        }

//        Generate the pepper for the password
        String pepperedPassword = getPepperedPassword(password);

//        Check the password with bcrypt and return the result;
        return BCrypt.checkpw(pepperedPassword, user.getPasswordHash());
    }

    private static String getPasswordHash(String password) {
        return BCrypt.hashpw(getPepperedPassword(password), BCrypt.gensalt());
    }

    private static String getPepperedPassword(String password) {
        return Hashing.sha256().hashString((password + hashPepper), StandardCharsets.UTF_8).toString();
    }

//    Made private, we don't allow registrations for now.
    @SuppressWarnings("unused")
    public static void registerUser(String username, String password) {
        UserEntity user = new UserEntity();
        user.setUsername(username);
        user.setPasswordHash(getPasswordHash(password));
        userDao.saveOrUpdateEntity(user);
    }
}
