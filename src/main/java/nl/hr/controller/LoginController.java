package nl.hr.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.hr.util.Path;
import nl.hr.util.ViewUtil;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.HashMap;
import java.util.Map;

import static nl.hr.util.RequestUtil.removeSessionAttrLoggedOut;

public class LoginController {

    public static final Route serveLoginPage = (Request request, Response response) -> {
        Map<String, Object> model = new HashMap<>();
        model.put("loggedOut", removeSessionAttrLoggedOut(request));
        return ViewUtil.render(request, model, Path.Template.LOGIN);
    };

    public static final Route handleLoginPost = (Request request, Response response) -> {
        response.status(200);
        response.type("application/json");

        Map<String, String> result = new HashMap<>();

        if (!UserController.authenticate(request.queryParams("username"), request.queryParams("password"))) {
            result.put("result", "authenticationFailed");
        } else {
            request.session().attribute("currentUser", request.queryParams("username"));
            result.put("result", "authenticationSucceeded");

            if (request.session().attribute("loginRedirect") != null) {
                result.put("loginredirect", request.session().attribute("loginRedirect"));
                request.session().removeAttribute("loginRedirect");
            }
        }

        ObjectMapper mapperObj = new ObjectMapper();
        return mapperObj.writeValueAsString(result);

    };

    public static final Route handleLogoutPost = (Request request, Response response) -> {
        request.session().removeAttribute("currentUser");
        request.session().attribute("loggedOut", true);
        response.redirect(Path.Web.LOGIN);
        return null;
    };

    // The origin of the request (request.pathInfo()) is saved in the session so
    // the user can be redirected back after login
    public static void ensureUserIsLoggedIn(Request request, Response response) {
        if (request.session().attribute("currentUser") == null) {
            request.session().attribute("loginRedirect", request.pathInfo());
            response.redirect(Path.Web.LOGIN);
        }
    }

}
