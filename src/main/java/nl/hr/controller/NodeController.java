package nl.hr.controller;

import nl.hr.dao.NodeDao;
import nl.hr.model.NodeEntity;
import nl.hr.util.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.thethingsnetwork.data.common.Message;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static nl.hr.Application.nodeDao;
import static nl.hr.controller.LoginController.ensureUserIsLoggedIn;

public class NodeController {
    public static final HashMap<Integer, NodeEntity> nodes = new HashMap<>();

    public NodeController() {
        init();
    }

    private void init() {
        NodeDao nodeDao = new NodeDao();
        for(NodeEntity node : (ArrayList<NodeEntity>) nodeDao.getAll(0)) {
            node.register();
            nodes.put(node.getId(), node);
        }
    }

    @SuppressWarnings("unused")
    public void callMessage(Message m) {
        nodes.forEach((nodeId, nodeEntity) -> nodeEntity.onMessage(m));
    }

    public static final Route serveNodesPage = (Request request, Response response) -> {
        ensureUserIsLoggedIn(request, response);
        Map<String, Object> model = new HashMap<>();
        model.put("nodes", nodes);
        model.put("nodesAsJson", getNodesAsJsonArray(nodes).toString());
        return ViewUtil.render(request, model, Path.Template.NODES);
    };

    public static final Route postNodePage = (Request request, Response response) -> {
        RequestUtil.validateCSRFToken(request);
        MessageBundle messages = request.attribute("messageBundle");

        if(request.queryParams("action") != null) {
            JSONArray errorArray;
            switch (request.queryParams("action")){
                case "update":
                    errorArray = getNodeFormErrors(request);

                    if(errorArray.length() == 0) {
                        NodeEntity node = (NodeEntity) nodeDao.getEntityById(Integer.valueOf(request.queryParams("data[id]")));

                        if(node == null) {
                            errorArray.put(new JSONObject("{ general : \"" + messages.get("VALIDATION_NODE_NOT_EXISTS") + "\"}"));
                            return errorArray;
                        }

                        node.setAppid(request.queryParams("data[appid]"));
                        node.setAppaccesskey(request.queryParams("data[appaccesskey]"));
                        node.setLatitude(DataTypeUtil.stringtoBigDecimal(request.queryParams("data[latitude]")));
                        node.setLongitude(DataTypeUtil.stringtoBigDecimal(request.queryParams("data[longitude]")));

                        nodeDao.updateEntity(node);

                        return new JSONObject("{'result' : 'updated'}");
                    }

                return errorArray;

                case "insert":
                    errorArray = getNodeFormErrors(request);

                    if(errorArray.length() == 0) {
                        NodeEntity node = new NodeEntity();

                        node.setAppid(request.queryParams("data[appid]"));
                        node.setAppaccesskey(request.queryParams("data[appaccesskey]"));
                        node.setLatitude(DataTypeUtil.stringtoBigDecimal(request.queryParams("data[latitude]")));
                        node.setLongitude(DataTypeUtil.stringtoBigDecimal(request.queryParams("data[longitude]")));

                        nodeDao.insertNode(node);

                        return new JSONObject("{'result' : 'inserted', 'nodeId' : " + node.getId() + "}");
                    }

                return errorArray;

                case "view":
                    if(request.queryParams("nodeId") != null && DataTypeUtil.stringIsInt(request.queryParams("nodeId"))) {
                        NodeEntity node = (NodeEntity) nodeDao.getEntityById(Integer.parseInt(request.queryParams("nodeId")));
                        return node.getAsJson();
                    }
                return false;

                case "delete":
                    if(request.queryParams("nodeId") != null && DataTypeUtil.stringIsInt(request.queryParams("nodeId"))){
                        nodeDao.deleteEntityById(Integer.parseInt(request.queryParams("nodeId")));
                        nodes.remove(Integer.parseInt(request.queryParams("nodeId")));
                        return true;
                    }
                return false;
            }
        }

        return false;
    };

    private static JSONArray getNodeFormErrors(Request request){
        MessageBundle messages = request.attribute("messageBundle");
        JSONArray errorArray = new JSONArray();
        try {
            if("".equals(request.queryParams("data[appid]"))) {
                errorArray.put(new JSONObject("{ appid : '" + messages.get("VALIDATION_FIELD_REQUIRED") + "' }"));
            } else if("".equals(request.queryParams("data[appaccesskey]"))) {
                errorArray.put(new JSONObject("{ appaccesskey : '" + messages.get("VALIDATION_FIELD_REQUIRED") + "' }"));
            } else if("".equals(request.queryParams("data[latitude]"))) {
                errorArray.put(new JSONObject("{ latitude : '" + messages.get("VALIDATION_FIELD_REQUIRED") + "' }"));
            } else if("".equals(request.queryParams("data[longitude]"))) {
                errorArray.put(new JSONObject("{ longitude : '" + messages.get("VALIDATION_FIELD_REQUIRED") + "' }"));
            } else {
                if(!"".equals(request.queryParams("data[id]")) && !DataTypeUtil.stringIsInt(request.queryParams("data[id]"))){
                    errorArray.put(new JSONObject("{ id : '" + messages.get("VALIDATION_FIELD_NUMBER") + "' }"));
                }
                if(request.queryParams("data[appid]").length() > 32) {
                    errorArray.put(new JSONObject("{ appid : '" + messages.get("VALIDATION_FIELD_LENGTH_32") + "' }"));
                }
                if(request.queryParams("data[appaccesskey]").length() > 64) {
                    errorArray.put(new JSONObject("{ appaccesskey : '" + messages.get("VALIDATION_FIELD_LENGTH_64") + "' }"));
                }
                if(DataTypeUtil.stringIsLatOrLong(request.queryParams("data[latitude]"))) {
                    errorArray.put(new JSONObject("{ latitude : '" + messages.get("VALIDATION_FIELD_LAT_LONG") + "' }"));
                }
                if(DataTypeUtil.stringIsLatOrLong(request.queryParams("data[longitude]"))) {
                    errorArray.put(new JSONObject("{ longitude : '" + messages.get("VALIDATION_FIELD_LAT_LONG") + "' }"));
                }
            }
        } catch (JSONException ignored) {
        }

        return errorArray;
    }

    private static JSONArray getNodesAsJsonArray(HashMap<Integer, NodeEntity> nodelist) {
        JSONArray jsonArray = new JSONArray();
        nodelist.forEach((nodeId, nodeEntity) -> jsonArray.put(nodeEntity.getAsJson()));
        return jsonArray;
    }
}