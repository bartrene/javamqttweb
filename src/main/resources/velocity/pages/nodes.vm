#parse("/velocity/layout.vm")

#@extraStyleSheets()
#end

#@mainLayout()

    #parse("/velocity/header.vm")

<!-- BEGIN BASE-->
<div id="base">

    <!-- BEGIN CONTENT-->
    <div id="content">
        <section class="fill-no-footer no-padding">
            <div id="map_canvas" class="fill"></div>
        </section>

        <section class="style-default-bright stickyBottom no-y-padding" style="width:calc(100% - 64px);">
            <div class="row row-eq-height">
                <div class="col-md-10">
                    <h2 class="text-primary">Nodes</h2>
                </div>
                <div class="col-md-2 text-right">
                    <button id="new-node-button" type="button" class="btn h2-margin ink-reaction btn-raised btn-primary">New Node</button>
                </div>
            </div>
            <table class="table table-hover node-table">
                <thead>
                <tr>
                    <th>AppId</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Last received</th>
                    <th class="text-right">Actions</th>
                </tr>
                </thead>
                <tbody>
                    #foreach( $n in $nodes )
                    <tr>
                        <td>$n.getAppid()</td>
                        <td>$n.getLatitude()</td>
                        <td>$n.getLongitude()</td>
                        <td>$!n.getLastReceived()</td>
                        <td class="text-right">
##                            <button type="button" class="btn btn-icon-toggle view-node" data-toggle="tooltip" data-placement="top" data-original-title="View row" ><i class="fa fa-bar-chart"></i></button>
                            <button type="button" class="btn btn-icon-toggle edit-node" data-toggle="tooltip" data-placement="top" data-original-title="Edit row"><i class="fa fa-pencil"></i></button>
                            <button type="button" class="btn btn-icon-toggle delete-node" data-toggle="tooltip" data-placement="top" data-original-title="Delete row"><i class="fa fa-trash-o"></i></button>
                        </td>
                    </tr>
                    #end
                </tbody>
            </table>
        </section>
    </div><!--end #content-->
    <!-- END CONTENT -->

    <!-- BEGIN SIDEBAR-->
    #parse("/velocity/sidebar.vm")
    <!-- END SIDEBAR -->


</div><!--end #base-->
<!-- END BASE -->

#end

#@footerScripts()
    <!-- BEGIN JAVASCRIPT -->
    <script src="/js/libs/bootbox/bootbox.min.js"></script>
    <script src="/js/libs/moment/moment.min.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCXfNqQgScR_XPzoPUmS7Jh-JqLEwdtXU&callback=initMap"></script>
    <script type="text/javascript">
        var CSRFToken = "$CSRFToken";
        var editingRow = "";
        var nodes = $nodesAsJson;
        var form =
            $('<form class="form" id="node-edit">'+
                '<div class="form-group">'+
                    '<input type="number" class="form-control" id="id" name="id" readonly>'+
                    '<label for="id">Node id</label>'+
                '</div>'+
                '<div class="form-group">'+
                    '<input type="text" class="form-control" id="appid" name="appid">'+
                    '<label for="appid">App id</label>'+
                '</div>'+
                '<div class="form-group">'+
                    '<input type="text" class="form-control" id="appaccesskey" name="appaccesskey">'+
                    '<label for="appaccesskey">App accesskey</label>'+
                '</div>'+
                '<div class="form-group">'+
                    '<input type="text" class="form-control" id="latitude" name="latitude">'+
                    '<label for="latitude">Latitude</label>'+
                '</div>'+
                '<div class="form-group">'+
                    '<input type="text" class="form-control" id="longitude" name="longitude">'+
                    '<label for="longitude">Longitude</label>'+
                '</div>'+
                '<div class="form-group">'+
                    '<input type="text" class="form-control" id="lastReceived" name="lastReceived" data-inputmask="\'mask\': \'d/m/y\'" readonly>'+
                    '<label for="lastReceived">LastReceived</label>'+
                '</div>'+
                '<div class="card-actionbar-row">'+
                    '<button type="submit" class="btn btn-flat btn-primary ink-reaction">Save node</button>'+
                '</div>'+
            '</form>');

        function initMap() {
            // Create the map.
            var map = new google.maps.Map(document.getElementById('map_canvas'), {
                maxZoom: 16,
                center: {lat: 51.918170, lng: 4.229822},
                mapTypeId: 'satellite',
                disableDefaultUI: true,
                disableDoubleClickZoom: true
            });

            var markers = [];//some array
            for(var node in nodes) {
                node = nodes[node];
                var icon = "../img/marker-red.png";
//                Check if the node was active within the last 60 minutes;
                if(moment.unix(node.lastReceived).diff(moment().add(-60, 'minute'), 'minutes') > 0) {
                    icon = "../img/marker-green.png";
                }

                var marker = createMarker({
                    map: map,
                    position: {lat: node.latitude, lng: node.longitude},
                    icon: icon
                }, "<p>" + node.appid + "</p>");
                markers.push(marker);
            }

//          Fit the map to the markers
            var bounds = new google.maps.LatLngBounds();
            for (marker in markers) {
                marker = markers[marker];
                bounds.extend(marker.getPosition());
            }
            map.fitBounds(bounds);
        }

        function createMarker(options, html) {
            var marker = new google.maps.Marker(options);
            var infoWindow = new google.maps.InfoWindow();
            infoWindow.setContent(html);
            google.maps.event.addListener(marker, "click", function () {
                infoWindow.open(options.map, this);
            });
            return marker;
        }

        $("#new-node-button").on("click", function (event) {
//            Clear all values
            form.find("#id").attr("value", "");
            form.find("#appid").attr("value", "");
            form.find("#appaccesskey").attr("value", "");
            form.find("#latitude").attr("value", "");
            form.find("#longitude").attr("value", "");
            form.find("#lastReceived").attr("value", "");

            bootbox.dialog({
                title: "Create node",
                message: form.prop('outerHTML')
            });
        });

        $(document).on('submit', 'form#node-edit', function( event ){
            var sendData = {};
            $($(this).serializeArray()).each(function(index, obj){
                sendData[obj.name] = obj.value;
            });

            if(sendData.id === "") {
                insertNode(sendData, $(this));
            } else {
                updateNode(sendData, $(this));
            }

            event.preventDefault();
            event.stopPropagation();
        });

        $(document).on('click', '.node-table .edit-node', function( event ){
            var nodeId = $(this).parent().attr("nodeid");
            var row = $(this).parents('tr');
            $.post( "$WebPath.getNODES()", { action : "view", nodeId : nodeId, CSRFToken: CSRFToken}, function( data ) {
                if(data) {
                    data = $.parseJSON(data);

                    form.find("#id").attr("value", data.id);
                    form.find("#appid").attr("value", data.appid);
                    form.find("#appaccesskey").attr("value", data.appaccesskey);
                    form.find("#latitude").attr("value", data.latitude);
                    form.find("#longitude").attr("value", data.longitude);
                    form.find("#lastReceived").attr("value", moment.unix(data.lastReceived).format("DD-MM-YYYY"));

                    editingRow = row;

                    bootbox.dialog({
                        title: "Edit node",
                        message: form.prop('outerHTML')
                    });
                }
            });
        });

        $(document).on('click', '.node-table .delete-node', function( event ){
            var nodeId = $(this).parent().attr("nodeid");
            var row = $(this).parents("tr");
            bootbox.confirm("Are you sure you wish to delete this node?", function(result){
                if(result) {
//                    Delete the node
                    $.post( "$WebPath.getNODES()", { action : "delete", nodeId : nodeId, CSRFToken: CSRFToken}, function( data ) {
                        if(data) {
                            $(nodes).each(function(index, node){
                                if(node.id === nodeId) {
                                    delete nodes[index];
                                    initMap();
                                    return;
                                }
                            });
                            row.fadeOut();
                        }
                    });
                }
            });
        });

        $(".modal").on('hidden.bs.modal', function () {
            $(this).data('bs.modal', null);
        });

        function insertNode(sendData, submittedForm) {
            $.post( "$WebPath.getNODES()", { action : "insert", data : sendData, CSRFToken: CSRFToken}, function( data ) {
                data = $.parseJSON(data);
                if(!"result" in data) {
                    showFormErrors(data);
                } else {
                    var newRow = $("table.node-table").find("tbody tr").first().clone();
                    var columns = newRow.find("td");
                    columns.eq(0).html(sendData.appid);
                    columns.eq(1).html(sendData.latitude);
                    columns.eq(2).html(sendData.longitude);
                    columns.eq(3).html("");
                    columns.eq(4).attr("nodeID", data.nodeId);
                    newRow.hide().prependTo($("table.node-table")).fadeIn();
                    submittedForm.parents(".modal").modal('hide');

                    // Cast position to int and redraw map
                    sendData.latitude = parseFloat(sendData.latitude);
                    sendData.longitude = parseFloat(sendData.longitude);
                    nodes.push(sendData);
                    initMap();
                }
            });
        }

        function updateNode(sendData, submittedForm) {
            $.post( "$WebPath.getNODES()", { action : "update", data : sendData, CSRFToken: CSRFToken}, function( data ) {
                console.log(data);
                data = $.parseJSON(data);
                if(!"result" in data) {
                    showFormErrors(data);
                } else {
                    var columns = editingRow.find("td");
                    columns.eq(0).html(sendData.appid);
                    columns.eq(1).html(sendData.latitude);
                    columns.eq(2).html(sendData.longitude);
                    columns.eq(4).attr("nodeID", sendData.nodeId);
                    submittedForm.parents(".modal").modal('hide');

                    $(nodes).each(function(index, node){
                        if(node.id === sendData.id) {
                            node.latitude = parseFloat(sendData.latitude);
                            node.longitude = parseFloat(sendData.longitude);
                            initMap();
                            return;
                        }
                    });
                }
            });
        }

        function showFormErrors(submittedForm, errors) {
            $(errors).each(function(index, obj){
                var element = Object.keys(obj)[0];
                var formGroup = submittedForm.find("#" + element).parents(".form-group");
                if(!formGroup.hasClass('has-error')){
                    formGroup.addClass('has-error');
                }
                submittedForm.find("#" + element).after($('<span id="' + element + '-error" class="help-block">' + obj[element] + '</span>'));
            });
        }
    </script>
#end